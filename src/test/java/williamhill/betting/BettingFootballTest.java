package williamhill.betting;


import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import williamhill.pages.betting.betslip.BetSlipData;
import williamhill.pages.betting.facade.BettingPageFacade;
import williamhill.pages.betting.footballpage.BettingFootballData;
import williamhill.webdriver.FirefoxDriverManager;

import java.math.BigDecimal;

/**
 * Created by alexandre.
 */
public class BettingFootballTest {


    @BeforeMethod
    public void setUp(){
        FirefoxDriverManager.StartDriver(10);
    }
    @AfterMethod
    public void tearDown(){
        FirefoxDriverManager.StopDriver();
    }

    @Test()
    public void bet1(){

        BettingFootballData bettingFootballData = new BettingFootballData("Santos");
        BetSlipData betSlipData = new BetSlipData(new BigDecimal("0.05"));

        new BettingPageFacade().doBet(bettingFootballData, betSlipData);
    }
    @Test()
    public void bet2(){

        BettingFootballData bettingFootballData = new BettingFootballData("Santos");
        BetSlipData betSlipData = new BetSlipData(new BigDecimal("0.30"));

        new BettingPageFacade().doBet(bettingFootballData, betSlipData);
    }
}
