package williamhill.webdriver;

import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by alexandre.
 */
public class FirefoxDriverManager extends BaseWebDriver {



    public static void StartDriver( int defaultTimeOut){

        System.setProperty("webdriver.gecko.driver", "./src/main/resources/geckodriver");
        DRIVER = new FirefoxDriver();
        DRIVER_WAIT = new WebDriverWait(DRIVER, defaultTimeOut);
    }


    public static void StopDriver() {
        DRIVER.quit();
    }

}
