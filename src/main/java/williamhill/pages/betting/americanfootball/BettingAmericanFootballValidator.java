package williamhill.pages.betting.americanfootball;

import williamhill.pages.base.BasePageValidator;

/**
 * Created by alexandre.
 */
public class BettingAmericanFootballValidator extends BasePageValidator<BettingAmericanFootballElementMap> {

    protected BettingAmericanFootballValidator(BettingAmericanFootballElementMap map) {
        super(map);
    }
}
