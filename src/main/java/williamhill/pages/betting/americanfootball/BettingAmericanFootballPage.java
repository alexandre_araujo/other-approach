package williamhill.pages.betting.americanfootball;

import williamhill.pages.base.BasePage;

/**
 * Created by alexandre.
 */
public class BettingAmericanFootballPage extends BasePage<BettingAmericanFootballElementMap, BettingAmericanFootballValidator>{

    protected BettingAmericanFootballPage(String url) {
        super(url);
    }
}
