package williamhill.pages.betting.facade;

import williamhill.pages.betting.betslip.BetSlipData;
import williamhill.pages.betting.betslip.BetSlipPage;
import williamhill.pages.betting.footballpage.BettingFootballData;
import williamhill.pages.betting.footballpage.BettingFootballPage;

/**
 * Created by alexandre.
 */
public class BettingPageFacade {

    private String url = "http://sports.williamhill.com/betting/en-gb";

    private BettingFootballPage bettingFootballPage = new BettingFootballPage(url);
    private BetSlipPage betSlipPage = new BetSlipPage(url);

    private BettingFootballPage getBettingFootballPage() {
        return bettingFootballPage;
    }

    private BetSlipPage getBetSlipPage() {
        return betSlipPage;
    }

    public void doBet(BettingFootballData footballData, BetSlipData slipData){

        getBettingFootballPage().navigate(); //1 Navigate to http://sports.williamhill.com/betting/en-gb

        getBettingFootballPage().getValidate().doesHomeTeamExists(footballData.getHomeTeamName()); //2 Navigate to any football event

        getBettingFootballPage().goToAFootballEvent(footballData); //2 Navigate to any football event

        getBettingFootballPage().betForHomeTeam(footballData); //3 Select event and place a £0.05 bet for the home team to ‘Win’

        getBetSlipPage().putBetValue(slipData.getBetAmount()); //4 Place bet and assert the odds and returns offered

        getBetSlipPage().getValidate().validateToReturnAmount(); //5 Parameterise the betslip stake so any monetary value can be entered
    }
}
