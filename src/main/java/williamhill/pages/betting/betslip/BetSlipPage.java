package williamhill.pages.betting.betslip;

import williamhill.pages.base.BasePage;

import java.math.BigDecimal;

/**
 * Created by alexandre.
 */
public class BetSlipPage extends BasePage<BetSlipPageElementMap, BetSlipValidator>{

    public BetSlipPage(String url) {
        super(url);
        this.map = new BetSlipPageElementMap();
        this.validate = new BetSlipValidator(this.map);
    }

    public BetSlipPage putBetValue(BigDecimal value){
        this.map.waitPageLoad();
        if(this.validate.mobileBetSlip()) this.map.getBetSlipMobileIconCount().click();

        this.map.getStakeInputField().sendKeys(value.toString());
        return this;
    }
}
