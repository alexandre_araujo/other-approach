package williamhill.pages.betting.betslip;


import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import williamhill.pages.base.BasePageElementMap;

import java.util.function.Function;

import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Created by alexandre.
 */
public class BetSlipPageElementMap extends BasePageElementMap {


    WebElement getStakeInputField(){
        fluentWait().until(element -> browser.findElement(By.xpath("//*[contains(@id, 'stake-input__')]")));
        return browser.findElement(By.xpath("//*[contains(@id, 'stake-input__')]"));
    }

    WebElement getToReturn() {
        return browser.findElement(By.id("total-to-return-price"));
    }

    WebElement getTotalStake() {
        return browser.findElement(By.id("total-stake-price"));
    }

    WebElement getBetPrice(){
        return browser.findElement(By.xpath("//*[contains(@id,'bet-price_')]"));
    }

    public WebElement getPlaceABetButton(){
        return browser.findElement(By.xpath("//*[@data-ng-click='placeBet()']"));
    }

    WebElement getBetSlipMobileIconCount() {
        return browser.findElement(By.id("mobile-betslip-count"));
    }
}
