package williamhill.pages.betting.betslip;



import williamhill.pages.base.BasePageValidator;

import java.math.BigDecimal;

/**
 * Created by alexandre.
 */
public class BetSlipValidator extends BasePageValidator<BetSlipPageElementMap> {


    BetSlipValidator(BetSlipPageElementMap map) {
        super(map);
    }

    boolean mobileBetSlip(){
       return this.map.getBetSlipMobileIconCount().isDisplayed();
    }

    public boolean validateToReturnAmount() {


        BigDecimal totalStake =new BigDecimal(this.map.getTotalStake().getText());
        BigDecimal toReturn =new BigDecimal(this.map.getToReturn().getText());

        String betPrice = this.map.getBetPrice().getText();

        //TO DO, understand how is calculated;
        return true;
    }
}
