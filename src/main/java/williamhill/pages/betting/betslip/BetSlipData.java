package williamhill.pages.betting.betslip;

import java.math.BigDecimal;

/**
 * Created by alexandre.
 */
public class BetSlipData {

    private final BigDecimal betAmount;

    public BetSlipData(BigDecimal betAmount) {
        this.betAmount = betAmount;
    }

    public BigDecimal getBetAmount() {
        return betAmount;
    }
}
