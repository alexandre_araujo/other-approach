package williamhill.pages.betting.footballpage;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import williamhill.pages.base.BasePageElementMap;


import java.util.List;

/**
 * Created by alexandre.
 */
public class BettingFootballElementMap extends BasePageElementMap {



    public List<WebElement> getFootballBetList() {
        return browser.findElements(By.xpath("//*/div[contains(@id, 'OB_EV')]"));
    }

    public WebElement BetOnHomeTeamButton(String homeTeam){

       for(WebElement element : this.getFootballBetList()){
           if(element.getText().contains(homeTeam)){
            return   element.findElements(By.tagName("div")).get(9);
           }
       }
        return this.getFootballBetList().get(0).findElements(By.tagName("div")).get(9);
    }



    public WebElement scrollToTeamElement(WebElement homeTeam){
        JavascriptExecutor jse = (JavascriptExecutor)browser;
        jse.executeScript("window.scrollBy(0,"+(homeTeam.getLocation().getY()-100)+");");

        return homeTeam;
    }



}
