package williamhill.pages.betting.footballpage;


import org.junit.Assert;
import williamhill.pages.base.BasePageValidator;


/**
 * Created by alexandre.
 */
public class BettingFootballValidator extends BasePageValidator<BettingFootballElementMap> {


    BettingFootballValidator(BettingFootballElementMap map) {
        super(map);
    }

    public void doesHomeTeamExists(String teamName ) {
        Assert.assertTrue("Team: "+teamName+" is not available for bet",
                this.map.getFootballBetList().stream().anyMatch(x -> x.getText().contains(teamName)));
    }


}
