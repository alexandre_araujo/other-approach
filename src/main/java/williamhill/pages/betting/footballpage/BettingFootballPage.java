package williamhill.pages.betting.footballpage;

import org.openqa.selenium.WebElement;
import williamhill.pages.base.BasePage;
import williamhill.pages.betting.betslip.BetSlipPage;

/**
 * Created by alexandre.
 */
public class BettingFootballPage extends BasePage<BettingFootballElementMap, BettingFootballValidator> {

    public BettingFootballPage(String url) {
        super(url);
        this.map = new BettingFootballElementMap();
        this.validate = new BettingFootballValidator(this.map);
    }


    public BettingFootballPage goToAFootballEvent(BettingFootballData team){
        this.map.waitPageLoad();
        WebElement first = this.map.getFootballBetList()
                .stream()
                .filter(w -> w.getText().contains(team.getHomeTeamName()))
                .findFirst()
                .orElse(this.map.getFootballBetList().get(0));

        this.map.scrollToTeamElement(first);

        return this;
    }



    public BetSlipPage betForHomeTeam(BettingFootballData homeTeam){
        this.map.waitPageLoad();
        this.map.BetOnHomeTeamButton(homeTeam.getHomeTeamName()).click();
        return new BetSlipPage(url);
    }
}
