package williamhill.pages.betting.footballpage;

/**
 * Created by alexandre.
 */
public class BettingFootballData {

    private final String homeTeamName;

    public BettingFootballData(String homeTeamName) {
        this.homeTeamName = homeTeamName;
    }

    public String getHomeTeamName() {
        return homeTeamName;
    }
}
