package williamhill.pages.betting.tennispage;

import williamhill.pages.base.BasePage;

/**
 * Created by alexandre.
 */
public class BettingTennisPage extends BasePage<BettingTennisPageElementMap, BettingTennisPageValidator> {

    protected BettingTennisPage(String url) {
        super(url);
    }
}
