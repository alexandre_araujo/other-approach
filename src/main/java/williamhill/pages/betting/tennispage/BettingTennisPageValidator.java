package williamhill.pages.betting.tennispage;


import williamhill.pages.base.BasePageElementMap;
import williamhill.pages.base.BasePageValidator;

/**
 * Created by alexandre.
 */
public class BettingTennisPageValidator extends BasePageValidator<BettingTennisPageElementMap> {


    protected BettingTennisPageValidator(BettingTennisPageElementMap map) {
        super(map);
    }
}
