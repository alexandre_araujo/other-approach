package williamhill.pages.base;



/**
 * Created by alexandre.
 */
public class BasePage<M extends BasePageElementMap, V extends BasePageValidator> extends BasePageImpl<M> {

    protected V validate;

    protected BasePage(String url) {
        super(url);
    }

    public V getValidate() {
        return validate;
    }

}

