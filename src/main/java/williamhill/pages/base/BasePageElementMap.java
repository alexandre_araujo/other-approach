package williamhill.pages.base;


import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import williamhill.webdriver.BaseWebDriver;

import static java.util.concurrent.TimeUnit.SECONDS;


/**
 * Created by alexandre.
 */
public class BasePageElementMap {
    protected WebDriver browser;

    public BasePageElementMap(){
        this.browser = BaseWebDriver.DRIVER;
    }

    public void waitPageLoad(){
        WebDriverWait wait = new WebDriverWait(browser, 10);
        wait.until(ExpectedConditions
                .invisibilityOf(browser.findElement(By.xpath("//*[@id='wh-preloader']/div"))));
    }

    public Wait fluentWait(){
        return new FluentWait(browser)
                .withTimeout(20, SECONDS)
                .pollingEvery(5, SECONDS)
                .ignoring(NoSuchElementException.class);
    }



}
