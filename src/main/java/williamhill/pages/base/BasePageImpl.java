package williamhill.pages.base;



import williamhill.webdriver.BaseWebDriver;

/**
 * Created by alexandre.
 */
public class BasePageImpl<M extends BasePageElementMap> {

    protected String url;

    protected M map;

    public BasePageImpl(String url) {
        this.url = url;
    }


    public void navigate() {
        BaseWebDriver.DRIVER.navigate().to(this.url);
    }
}
